/**
 * @file stack_32_1_test.c
 * @brief
 * @author Easymov Robotics
 * @date 2017-03
 */

/* Includes ------------------------------------------------------------------*/

#include "unity.h"
#include "unity_fixture.h"

#include "stack.h"
#include "stack_test_utils.h"

/* Private constants ---------------------------------------------------------*/

#define BUFFER_SIZE 1u
#define FIFO_SIZE 32u
#define FRAME_MAX_LEN 16u
#define MAX_FRAME_IN_BUFFER 16u

/* Test ----------------------------------------------------------------------*/

GenericStack_t stack_32_1;

uint8_t stack_32_1_data[FIFO_SIZE][BUFFER_SIZE];
uint16_t stack_31_1_element_size[MAX_FRAME_IN_BUFFER];

Stack_data_t stack_32_1_settings = {
	.szElt = BUFFER_SIZE,
	.szStack = FIFO_SIZE,
	.stack = stack_32_1_data,
	.szEltSize = MAX_FRAME_IN_BUFFER,
	.eltSize = stack_31_1_element_size
};

/* Runner -------------------------------------------------------------------*/

TEST_GROUP_RUNNER(Stack_32_1)
{
	RUN_TEST_CASE(Stack_32_1, GetWriteElementOk);
	RUN_TEST_CASE(Stack_32_1, PushAndPopOneElement);
	RUN_TEST_CASE(Stack_32_1, WriteAndReadOneElement);
	RUN_TEST_CASE(Stack_32_1, WriteAndReadThreeElement);
	RUN_TEST_CASE(Stack_32_1, CheckGetNbElementsWhenWriteOneElement);
	RUN_TEST_CASE(Stack_32_1, WriteAndReadMultipleElementsNoOverflow);
	RUN_TEST_CASE(Stack_32_1, WriteAndReadMultipleElementsOverflowNoFrameLost);
	RUN_TEST_CASE(Stack_32_1, WriteAndReadMultipleElementsOverflowFrameLost);
}

TEST_GROUP(Stack_32_1);

TEST_SETUP(Stack_32_1)
{
	for (uint8_t i = 0 ; i < FIFO_SIZE ; ++i)
	{
		for (uint8_t j = 0 ; j < BUFFER_SIZE ; ++j)
		{
			stack_32_1_data[i][j] = 0u;
		}
	}

	Stack_Init(&stack_32_1, &stack_32_1_settings);
}

TEST_TEAR_DOWN(Stack_32_1)
{

}

TEST(Stack_32_1, PushAndPopOneElement)
{
	uint8_t size = 1u;
	uint8_t ret_val = 0u, nb_element = 0u;
	uint8_t element_to_push[size];
	uint8_t element_poped[size];

	FILL_DATA_ARRAY(element_to_push, 255, size);

	Stack_PushBack(&stack_32_1, element_to_push);
	nb_element = Stack_GetNbElements(&stack_32_1);

	TEST_ASSERT_EQUAL(1u, nb_element);

	ret_val = Stack_PopBack(&stack_32_1, element_poped);
	nb_element = Stack_GetNbElements(&stack_32_1);

	TEST_ASSERT_EQUAL(0u, nb_element);
	TEST_ASSERT_EQUAL(1u, ret_val);
	TEST_ASSERT_EQUAL_ARRAY(element_poped, element_to_push, size);
}

TEST(Stack_32_1, GetWriteElementOk)
{
	uint8_t ret_val = 0u, nb_element = 0u;
	uint8_t * buffer = 0u;

	ret_val = Stack_GetWriteElt(&stack_32_1, &buffer);
	TEST_ASSERT_EQUAL(1u, ret_val);

	ret_val = Stack_ReleaseMultipleWriteElt(&stack_32_1, 3u);
	TEST_ASSERT_EQUAL(1u, ret_val);
}

TEST(Stack_32_1, WriteAndReadOneElement)
{
	uint8_t ret_val = 0u;
	uint8_t * write_buffer = 0u;
	uint8_t * read_buffer = 0u;

	Stack_GetWriteElt(&stack_32_1, &write_buffer);
	Stack_ReleaseWriteElt(&stack_32_1);
	FILL_DATA_ARRAY(write_buffer, 255u, 1u);

	ret_val = Stack_GetReadElt(&stack_32_1, &read_buffer);
	TEST_ASSERT_EQUAL(1u, ret_val);

	ret_val = Stack_ReleaseReadElt(&stack_32_1);
	TEST_ASSERT_EQUAL(1u, ret_val);

	TEST_ASSERT_EQUAL_ARRAY(write_buffer, read_buffer, 1u);
}

TEST(Stack_32_1, WriteAndReadThreeElement)
{
	uint8_t size = 3u;
	uint8_t ret_val = 0u;
	uint8_t * write_buffer = 0u;
	uint8_t * read_buffer = 0u;

	Stack_GetWriteElt(&stack_32_1, &write_buffer);
	Stack_ReleaseMultipleWriteElt(&stack_32_1, size);
	FILL_DATA_ARRAY(write_buffer, 255u, size);

	ret_val = Stack_GetReadElt(&stack_32_1, &read_buffer);
	TEST_ASSERT_EQUAL(1u, ret_val);

	ret_val = Stack_ReleaseReadElt(&stack_32_1);
	TEST_ASSERT_EQUAL(1u, ret_val);

	TEST_ASSERT_EQUAL_ARRAY(write_buffer, read_buffer, size);
}

TEST(Stack_32_1, CheckGetNbElementsWhenWriteOneElement)
{
	uint8_t size = 3u;
	uint8_t nb_element = 0u;
	uint8_t * write_buffer = 0u;
	uint8_t * read_buffer = 0u;

	Stack_GetWriteElt(&stack_32_1, &write_buffer);
	Stack_ReleaseMultipleWriteElt(&stack_32_1, size);
	FILL_DATA_ARRAY(write_buffer, 255u, size);

	Stack_GetReadElt(&stack_32_1, &read_buffer);
	nb_element = Stack_GetNbElements(&stack_32_1);
	TEST_ASSERT_EQUAL(1u, nb_element);

	Stack_ReleaseReadElt(&stack_32_1);
	nb_element = Stack_GetNbElements(&stack_32_1);
	TEST_ASSERT_EQUAL(0u, nb_element);
}

TEST(Stack_32_1, WriteAndReadMultipleElementsNoOverflow)
{
	uint8_t * write_buffer = 0u;
	uint8_t * read_buffer = 0u;
	uint8_t nb_element = 0u;

	uint8_t frame_1[] = {170u, 170u, 170u};
	uint8_t frame_1_size = 3u;

	uint8_t frame_2[] = {121u, 121u};
	uint8_t frame_2_size = 2u;

	uint8_t frame_3[] = {155u, 155u, 155u, 155u};
	uint8_t frame_3_size = 4u;

	Stack_GetWriteElt(&stack_32_1, &write_buffer);
	Stack_ReleaseMultipleWriteElt(&stack_32_1, frame_1_size);
	COPY_ARRAY(write_buffer, frame_1, frame_1_size);

	Stack_GetWriteElt(&stack_32_1, &write_buffer);
	Stack_ReleaseMultipleWriteElt(&stack_32_1, frame_2_size);
	COPY_ARRAY(write_buffer, frame_2, frame_2_size);

	Stack_GetWriteElt(&stack_32_1, &write_buffer);
	Stack_ReleaseMultipleWriteElt(&stack_32_1, frame_3_size);
	COPY_ARRAY(write_buffer, frame_3, frame_3_size);

	nb_element = Stack_GetNbElements(&stack_32_1);
	TEST_ASSERT_EQUAL(3u, nb_element);

	Stack_GetReadElt(&stack_32_1, &read_buffer);
	Stack_ReleaseReadElt(&stack_32_1);
	TEST_ASSERT_EQUAL_ARRAY(frame_1, read_buffer, frame_1_size);

	Stack_GetReadElt(&stack_32_1, &read_buffer);
	Stack_ReleaseReadElt(&stack_32_1);
	TEST_ASSERT_EQUAL_ARRAY(frame_2, read_buffer, frame_2_size);

	Stack_GetReadElt(&stack_32_1, &read_buffer);
	Stack_ReleaseReadElt(&stack_32_1);
	TEST_ASSERT_EQUAL_ARRAY(frame_3, read_buffer, frame_3_size);
}

TEST(Stack_32_1, WriteAndReadMultipleElementsOverflowNoFrameLost)
{

}

TEST(Stack_32_1, WriteAndReadMultipleElementsOverflowFrameLost)
{
	uint8_t * write_buffer = 0u;
	uint8_t * read_buffer = 0u;
	uint8_t nb_element = 0u;
	uint8_t frame_lost = 0u;

	uint8_t frame_1[] = {170u, 170u, 170u, 170u, 170u, 170u, 170u, 170u, 170u};
	uint8_t frame_1_size = 9u;

	uint8_t frame_2[] = {121u, 121u, 121u, 121u, 121u, 121u, 121u, 121u, 121u, 121u, 121u, 121u};
	uint8_t frame_2_size = 12u;

	uint8_t frame_3[] = {155u, 155u, 155u, 155u, 155u, 155u, 155u, 155u, 155u, 155u, 155u, 155u, 155u, 155u, 155u, 155u};
	uint8_t frame_3_size = 16u;

	frame_lost = Stack_GetMultipleWriteElt(&stack_32_1, &write_buffer, FRAME_MAX_LEN);
	Stack_ReleaseMultipleWriteElt(&stack_32_1, frame_1_size);
	COPY_ARRAY(write_buffer, frame_1, frame_1_size);
	TEST_ASSERT_EQUAL(0u, frame_lost);

	frame_lost = Stack_GetMultipleWriteElt(&stack_32_1, &write_buffer, FRAME_MAX_LEN);
	Stack_ReleaseMultipleWriteElt(&stack_32_1, frame_2_size);
	COPY_ARRAY(write_buffer, frame_2, frame_2_size);
	TEST_ASSERT_EQUAL(0u, frame_lost);

	Stack_GetReadElt(&stack_32_1, &read_buffer);
	Stack_ReleaseReadElt(&stack_32_1);
	TEST_ASSERT_EQUAL_ARRAY(frame_1, read_buffer, frame_1_size);

	frame_lost = Stack_GetMultipleWriteElt(&stack_32_1, &write_buffer, FRAME_MAX_LEN);
	Stack_ReleaseMultipleWriteElt(&stack_32_1, frame_3_size);
	COPY_ARRAY(write_buffer, frame_3, frame_3_size);
	TEST_ASSERT_EQUAL(1u, frame_lost);

	Stack_GetReadElt(&stack_32_1, &read_buffer);
	Stack_ReleaseReadElt(&stack_32_1);
	TEST_ASSERT_EQUAL_ARRAY(frame_3, read_buffer, frame_3_size);
}
