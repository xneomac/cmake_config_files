/**
 * @file main.c
 * @brief
 * @author Easymov Robotics
 * @date 2017-03
 */

#include "unity.h"
#include "unity_fixture.h"

static void RunAllTests(void)
{
  RUN_TEST_GROUP(Stack_32_1);
  RUN_TEST_GROUP(Stack_2_120);
}

int main(int argc, const char ** argv)
{
  return UnityMain(argc, argv, RunAllTests);
}
