/**
 * @file stack_2_120_test.c
 * @brief
 * @author Easymov Robotics
 * @date 2017-03
 */

/* Includes ------------------------------------------------------------------*/

#include "unity.h"
#include "unity_fixture.h"

#include "stack.h"
#include "stack_test_utils.h"

/* Private constants ---------------------------------------------------------*/

#define BUFFER_SIZE 120u
#define FIFO_SIZE 2u
#define FRAME_MAX_LEN 16u
#define MAX_FRAME_IN_BUFFER 16u

/* Test ----------------------------------------------------------------------*/

GenericStack_t stack_2_120;

uint8_t stack_2_120_data[FIFO_SIZE][BUFFER_SIZE];
uint16_t stack_2_120_element_size[MAX_FRAME_IN_BUFFER];

Stack_data_t stack_2_120_settings = {
	.szElt = BUFFER_SIZE,
	.szStack = FIFO_SIZE,
	.stack = stack_2_120_data,
	.szEltSize = MAX_FRAME_IN_BUFFER,
	.eltSize = stack_2_120_element_size
};

/* Runner -------------------------------------------------------------------*/

TEST_GROUP_RUNNER(Stack_2_120)
{
	RUN_TEST_CASE(Stack_2_120, GetWriteElementOk);
	RUN_TEST_CASE(Stack_2_120, PushAndPopOneElement);
	RUN_TEST_CASE(Stack_2_120, WriteAndReadOneElement);
	RUN_TEST_CASE(Stack_2_120, CheckGetNbElementsWhenWriteOneElement);
}

TEST_GROUP(Stack_2_120);

TEST_SETUP(Stack_2_120)
{
	for (uint8_t i = 0 ; i < FIFO_SIZE ; ++i)
	{
		for (uint8_t j = 0 ; j < BUFFER_SIZE ; ++j)
		{
			stack_2_120_data[i][j] = 0u;
		}
	}

	Stack_Init(&stack_2_120, &stack_2_120_settings);
}

TEST_TEAR_DOWN(Stack_2_120)
{

}

TEST(Stack_2_120, PushAndPopOneElement)
{
	uint8_t ret_val = 0u, nb_element = 0u;
	uint8_t element_to_push[BUFFER_SIZE];
	uint8_t element_poped[BUFFER_SIZE];

	FILL_DATA_ARRAY(element_to_push, 137, BUFFER_SIZE);

	Stack_PushBack(&stack_2_120, element_to_push);
	nb_element = Stack_GetNbElements(&stack_2_120);

  PRINT_STACK(&stack_2_120, FIFO_SIZE * BUFFER_SIZE);

	TEST_ASSERT_EQUAL(1u, nb_element);

	ret_val = Stack_PopBack(&stack_2_120, element_poped);
	nb_element = Stack_GetNbElements(&stack_2_120);

	TEST_ASSERT_EQUAL(0u, nb_element);
	TEST_ASSERT_EQUAL(1u, ret_val);
	TEST_ASSERT_EQUAL_ARRAY(element_poped, element_to_push, BUFFER_SIZE);
}

TEST(Stack_2_120, GetWriteElementOk)
{
	uint8_t ret_val = 0u, nb_element = 0u;
	uint8_t * buffer = 0u;

	ret_val = Stack_GetWriteElt(&stack_2_120, &buffer);
	TEST_ASSERT_EQUAL(1u, ret_val);

	ret_val = Stack_ReleaseMultipleWriteElt(&stack_2_120, 3u);
	TEST_ASSERT_EQUAL(1u, ret_val);
}

TEST(Stack_2_120, WriteAndReadOneElement)
{
	uint8_t ret_val = 0u;
	uint8_t * write_buffer = 0u;
	uint8_t * read_buffer = 0u;

	Stack_GetWriteElt(&stack_2_120, &write_buffer);
	Stack_ReleaseWriteElt(&stack_2_120);
	FILL_DATA_ARRAY(write_buffer, 255u, 1u);

	ret_val = Stack_GetReadElt(&stack_2_120, &read_buffer);
	TEST_ASSERT_EQUAL(1u, ret_val);

	ret_val = Stack_ReleaseReadElt(&stack_2_120);
	TEST_ASSERT_EQUAL(1u, ret_val);

	TEST_ASSERT_EQUAL_ARRAY(write_buffer, read_buffer, 1u);
}

TEST(Stack_2_120, CheckGetNbElementsWhenWriteOneElement)
{
	uint8_t size = 3u;
	uint8_t nb_element = 0u;
	uint8_t * write_buffer = 0u;
	uint8_t * read_buffer = 0u;

	Stack_GetWriteElt(&stack_2_120, &write_buffer);
	Stack_ReleaseMultipleWriteElt(&stack_2_120, size);
	FILL_DATA_ARRAY(write_buffer, 255u, size);

	Stack_GetReadElt(&stack_2_120, &read_buffer);
	nb_element = Stack_GetNbElements(&stack_2_120);
	TEST_ASSERT_EQUAL(1u, nb_element);

	Stack_ReleaseReadElt(&stack_2_120);
	nb_element = Stack_GetNbElements(&stack_2_120);
	TEST_ASSERT_EQUAL(0u, nb_element);
}
