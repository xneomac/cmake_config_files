/**
 * @file stack_test_utils.h
 * @brief
 * @author Easymov Robotics
 * @date 2017-03
 */

/* Multi-include protection --------------------------------------------------*/
#ifndef __STACK_TEST_UTILS__
#define __STACK_TEST_UTILS__

#ifdef __cplusplus
extern "C" {
#endif

#include "stack.h"

void PRINT_STACK_INFO(GenericStack_t * pStack);
void PRINT_STACK(GenericStack_t * pStack, uint32_t fifo_size);
void COPY_ARRAY(uint8_t * array_1, uint8_t * array_2, uint32_t size);
void FILL_DATA_ARRAY(uint8_t * array, uint8_t data, uint32_t size);
void TEST_ASSERT_EQUAL_ARRAY(uint8_t * array_1, uint8_t * array_2, uint32_t size);

#ifdef __cplusplus
}
#endif

#endif /* __STACK_TEST_UTILS__ */
