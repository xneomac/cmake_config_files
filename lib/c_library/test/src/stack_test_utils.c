/**
 * @file stack_test_utils.c
 * @brief
 * @author Easymov Robotics
 * @date 2017-03
 */

#include <stdio.h>
#include "stack_test_utils.h"
#include "unity.h"

void PRINT_STACK_INFO(GenericStack_t * pStack)
{
	printf("\nwrite: %d\n", pStack->idxWr);
	printf("read: %d\n", pStack->idxRd);
	printf("nb: %d\n", pStack->nbElt);
}

void PRINT_STACK(GenericStack_t * pStack, uint32_t fifo_size)
{
	printf("\nwrite: %d\n", pStack->idxWr);
	printf("read: %d\n", pStack->idxRd);
	printf("nb: %d\n", pStack->nbElt);
	printf("data: ");
	for (uint8_t i = 0 ; i < fifo_size ; ++i) printf("%d ", ((*pStack->sData->stack)+i)[0]);
	for (uint8_t i = fifo_size ; i < fifo_size +10 ; ++i) printf("(%d) ", ((*pStack->sData->stack)+i)[0]);
	printf("\n");
}

void COPY_ARRAY(uint8_t * array_1, uint8_t * array_2, uint32_t size)
{
	for (uint8_t i = 0 ; i < size ; ++i)
	{
		array_1[i] = array_2[i];
	}
}

void FILL_DATA_ARRAY(uint8_t * array, uint8_t data, uint32_t size)
{
	for (uint8_t i = 0 ; i < size ; ++i) array[i] = data;
}

void TEST_ASSERT_EQUAL_ARRAY(uint8_t * array_1, uint8_t * array_2, uint32_t size)
{
	for (uint8_t i = 0 ; i < size ; ++i)
	{
		TEST_ASSERT_EQUAL(array_1[i], array_2[i]);
	}
}
