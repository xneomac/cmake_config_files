/******************** (C) COPYRIGHT 2015 exoTIC Systems ************************
* File Name          : mem_utils.h
* Description        : TODO
********************************************************************************/

/* Multi-include protection --------------------------------------------------*/
#ifndef __mem_utils_H__
#define __mem_utils_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/* Exported variables --------------------------------------------------------*/

/* Exported functions --------------------------------------------------------*/

void 		mem_utils_cpy(uint8_t dest[], const uint8_t src[], const uint16_t bytesToCopy);
uint16_t 	mem_utils_strlen(const uint8_t str[]);
void 		mem_utils_set(uint8_t str[], uint8_t defaultValue, const uint16_t bytesToSet);

#ifdef __cplusplus
}
#endif

#endif /* __mem_utils_H__ */
