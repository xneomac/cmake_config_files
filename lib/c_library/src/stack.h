
#ifndef __STACK_H__
#define __STACK_H__

#include <stdint.h>

typedef struct __STACK_DATA__ {
	  uint16_t szElt;
	  uint16_t szStack;
	  uint8_t (*stack)[];
		uint16_t szEltSize;
		uint16_t *eltSize;
} Stack_data_t;

/**
 *  Struct used to cast any stack in generic one
 */
typedef struct {
  uint32_t crc;
  uint16_t idxWr;
  uint16_t idxRd;
  uint16_t nbElt;

	uint16_t idxSizeWr;
	uint16_t idxSizeRd;

  Stack_data_t * sData;
} GenericStack_t;


uint8_t 	Stack_Init(GenericStack_t * pStack, Stack_data_t * pStackData);

uint8_t 	Stack_PushBack(GenericStack_t * pStack, const uint8_t * pElt);
uint16_t 	Stack_PushBackMultiElements(GenericStack_t * pStack, const uint8_t pElt[], uint16_t bytesToWrite);

uint8_t 	Stack_PopBack(GenericStack_t * pStack, uint8_t * pElt);
uint16_t 	Stack_PopBackMultiElements(GenericStack_t * pStack, uint8_t * pElt, uint16_t bytesToRead);

uint8_t 	Stack_Get(const GenericStack_t * pStack, uint8_t ** pElt, uint16_t indice);

uint8_t 	Stack_GetReadElt(GenericStack_t * pStack, uint8_t ** pElt);
uint8_t 	Stack_ReleaseReadElt(GenericStack_t * pStack);

uint8_t 	Stack_GetWriteElt(GenericStack_t * pStack, uint8_t ** pElt);
uint8_t 	Stack_GetMultipleWriteElt(GenericStack_t * pStack, uint8_t ** pElt, uint16_t size);
uint8_t 	Stack_ReleaseWriteElt(GenericStack_t * pStack);
uint8_t   Stack_ReleaseMultipleWriteElt(GenericStack_t * pStack, uint16_t size);

uint8_t 	Stack_GetLast(const GenericStack_t * pStack, uint8_t ** pElt);
uint16_t 	Stack_GetNbElements(const GenericStack_t * pStack);
uint8_t 	Stack_isFull(const GenericStack_t * pStack);

uint16_t 	Stack_CheckIntegrity(GenericStack_t * pStack);

#endif
