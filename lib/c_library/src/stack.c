#include "stack.h"

#include "mem_utils.h"

#include <stdio.h>

uint8_t Stack_GetOldest(const GenericStack_t * pStack, uint8_t ** pElt);

/*******************************************************************************
 * Function Name  : Stack_Init
 * Description    : Initialize stack variable and fill stack space with 0xFF
 * Input          : stack to configure.
 * Input          : size of element.
 * Input          : size of stack.
 * Return         : 1.
 *******************************************************************************/
uint8_t Stack_Init(GenericStack_t * pStack, Stack_data_t * pStackData)
{
	pStack->idxRd = 0U;
	pStack->idxWr = 0U;
	pStack->nbElt = 0U;
	pStack->sData = pStackData;

	pStack->idxSizeWr = 0u;
	pStack->idxSizeRd = 0u;

	return 1U;
}

/*******************************************************************************
 * Function Name  : Stack_PushBack
 * Description    : Push new data in stack.
 * Input          : Stack to use.
 * Input          : Element to push.
 * Return         : 1.
 *******************************************************************************/
uint8_t Stack_PushBack(GenericStack_t * pStack, const uint8_t * pElt)
{
	uint8_t * PushBack_pData_ = (uint8_t*) 0u;

	/* On pousse le pointeur de lecture */
	if((pStack->idxWr == pStack->idxRd) && (pStack->nbElt == pStack->sData->szStack))
	{
		pStack->idxRd = (((pStack->idxRd + 1U) < pStack->sData->szStack) ? (pStack->idxRd + 1U) : (0U));
	}

	PushBack_pData_ = (*(pStack->sData->stack));
	PushBack_pData_ += ( (pStack->idxWr) * (pStack->sData->szElt) );

	mem_utils_cpy(PushBack_pData_ ,
			pElt,
			(pStack->sData->szElt));

	pStack->idxWr = (((pStack->idxWr + 1U) < pStack->sData->szStack) ? (pStack->idxWr + 1U) : (0U));

	if(pStack->nbElt < pStack->sData->szStack)
	{
		++pStack->nbElt;
	}

#if 0
	pStack->crc = crc32(0, pStack->sData->stack, pStack->sData->szStack * pStack->sData->szElt);
#endif

	return 1U;
}

/*******************************************************************************
 * Function Name  : Stack_PushBackMultiElements
 * Description    : Push elements in stack
 * Input          : Stack to use.
 * Input          : Elements to push.
 * Input          : Number of elements to push
 * Return         : Number of elements pushed.
 *******************************************************************************/
uint16_t Stack_PushBackMultiElements(GenericStack_t * pStack, const uint8_t * pElt, uint16_t bytesToWrite)
{
	uint16_t pushed_len_ = 0u;

	for(pushed_len_ = 0u; pushed_len_ < bytesToWrite; pushed_len_++)
	{
		Stack_PushBack(pStack, pElt);

		pElt += pStack->sData->szElt;
	}

	return pushed_len_;
}

/*******************************************************************************
 * Function Name  : Stack_PopBack
 * Description    : Pop data from stack (data is removed after)
 * Input          : Stack to use.
 * Input          : Element output.
 * Return         : 1 if there is an element, 0 if stack empty.
 *******************************************************************************/
uint8_t Stack_PopBack(GenericStack_t * pStack, uint8_t * pElt)
{
	uint8_t PopBack_RetVal_ = 0u;
	uint8_t * PopBack_pData_ = (uint8_t *) 0u;

	if(pStack->nbElt > 0U)
	{
		PopBack_pData_ = (*(pStack->sData->stack));
		PopBack_pData_ += ( (pStack->idxRd) * (pStack->sData->szElt) );

		mem_utils_cpy(pElt,
				PopBack_pData_,
				(pStack->sData->szElt));

		pStack->idxRd = (((pStack->idxRd + 1U) < pStack->sData->szStack) ? (pStack->idxRd + 1U) : 0U);

		pStack->nbElt--;

#if 0
		pStack->crc = crc32(0,pStack->sData->stack, pStack->sData->szStack * pStack->sData->szElt);
#endif
		PopBack_RetVal_ = 1u;
	}

	return PopBack_RetVal_;
}

/*******************************************************************************
 * Function Name  : Stack_PopBackMultiElements
 * Description    : Pop multi elements in stack.
 * Input          : Stack to use.
 * Input          : Elements output.
 * Input          : Number of elements to pop
 * Return         : Number of elements copied.
 *******************************************************************************/
uint16_t 	Stack_PopBackMultiElements(GenericStack_t * pStack, uint8_t * pElt, uint16_t bytesToRead)
{
	uint16_t pop_len_ = 0u;

	for(pop_len_ = 0u; pop_len_ < bytesToRead; pop_len_++)
	{
		if(Stack_PopBack(pStack, pElt) == 0U)
		{
			break;
		}

		pElt += pStack->sData->szElt;
	}

	return pop_len_;
}

/*******************************************************************************
 * Function Name  : Stack_GetOldest
 * Description    : Get oldest element pushed (data remain in stack)
 * Input          : Stack to use.
 * Input          : Element output.
 * Return         : 1 if there is an element, 0 if stack empty.
 *******************************************************************************/
uint8_t Stack_GetOldest(const GenericStack_t * pStack, uint8_t ** pElt)
{
	uint8_t GetOldest_RetVal_ = 0u;

	if(pStack->nbElt > 0U) {

		*pElt = (*(pStack->sData->stack));
		*pElt += ((pStack->idxRd) * (pStack->sData->szElt));

		GetOldest_RetVal_ = 1u;
	}

	return GetOldest_RetVal_;
}

/*******************************************************************************
 * Function Name  : Stack_GetLast
 * Description    : Get Last element pushed (data remain in stack)
 * Input          : Stack to use.
 * Input          : Element output.
 * Return         : 1 if there is an element, 0 if stack empty.
 *******************************************************************************/
uint8_t Stack_GetLast(const GenericStack_t * pStack, uint8_t ** pElt)
{
	uint8_t GetLast_RetVal_ = 0u;

	if(pStack->idxRd != pStack->idxWr)
	{
		*pElt = (*(pStack->sData->stack));

		if(pStack->idxWr == 0U)
		{
			*pElt += ((pStack->sData->szStack - 1u) * (pStack->sData->szElt));
		}else{
			*pElt += ((pStack->idxWr - 1u) * (pStack->sData->szElt));
		}

		GetLast_RetVal_ = 1u;

	}

	return GetLast_RetVal_;
}

/*******************************************************************************
 * Function Name  : Stack_Get
 * Description    : Get element at indice (data remain in stack)
 * Input          : Stack to use.
 * Input          : Element output.
 * Input          : Indice of element in stack.
 * Return         : 1 if there is an element, 0 if stack empty.
 *******************************************************************************/
uint8_t Stack_Get(const GenericStack_t * pStack, uint8_t ** pElt, uint16_t indice)
{
	uint8_t Get_RetVal_ = 0u;

	if(indice > pStack->nbElt){
		Get_RetVal_ = 0U;
	}else{
		*pElt = (*(pStack->sData->stack));
		*pElt += (((pStack->idxRd + indice) % (pStack->sData->szStack)) * (pStack->sData->szElt));

		Get_RetVal_ = 1u;
	}

	return Get_RetVal_;
}

/*******************************************************************************
 * Function Name  : Stack_GetReadElt
 * Description    : Get element to read
 * Input          : Stack to use.
 * Input          : Element output.
 * Return         : 1 if there is an element, 0 if stack empty.
 *******************************************************************************/
uint8_t Stack_GetReadElt(GenericStack_t * pStack, uint8_t ** pElt)
{
	uint8_t GetRead_RetVal_ = 0u;

	if(pStack->nbElt > 0u)
	{
		*pElt = (*(pStack->sData->stack));
		*pElt += ((pStack->idxRd) * (pStack->sData->szElt));

		GetRead_RetVal_ = 1u;
	}

	return GetRead_RetVal_;
}

uint8_t Stack_ReleaseReadElt(GenericStack_t * pStack)
{
	uint8_t Release_RetVal_ = 0u;
	uint16_t size_ = pStack->sData->eltSize[pStack->idxSizeRd];
	pStack->idxSizeRd = (pStack->idxSizeRd + 1u) % pStack->sData->szEltSize;

	if(pStack->nbElt > 0U)
	{
		pStack->idxRd = (pStack->idxRd + size_) % (pStack->sData->szStack);
		pStack->nbElt--;

		Release_RetVal_ = 1u;
	}

	return Release_RetVal_;
}

/*******************************************************************************
 * Function Name  : Stack_GetWriteElt
 * Description    : Get element to write
 * Input          : Stack to use.
 * Input          : Element output.
 * Return         : 1 if there is an element, 0 if stack empty.
 *******************************************************************************/
uint8_t Stack_GetWriteElt(GenericStack_t * pStack, uint8_t ** pElt)
{
	uint8_t GetWrite_RetVal_ = 0u;

	*pElt = (*(pStack->sData->stack));
	*pElt += ((pStack->idxWr) * (pStack->sData->szElt));

	GetWrite_RetVal_ = 1u;

	return GetWrite_RetVal_;
}

uint8_t Stack_GetMultipleWriteElt(GenericStack_t * pStack, uint8_t ** pElt, uint16_t size)
{
	uint8_t frame_lost_ = 0u;
	uint16_t available_space_ = (pStack->sData->szStack - pStack->idxWr);

	if (available_space_ <= size)
	{
		while (pStack->nbElt > 0 && pStack->idxRd < size)
		{
			Stack_ReleaseReadElt(pStack);
			frame_lost_ += 1;
		}

		if (pStack->nbElt == 0)
		{
			pStack->idxRd = 0;
		}

		pStack->idxWr = 0;
	}

	Stack_GetWriteElt(pStack, pElt);

	return frame_lost_;
}

uint8_t Stack_ReleaseWriteElt(GenericStack_t * pStack)
{
	return Stack_ReleaseMultipleWriteElt(pStack, 1u);
}

uint8_t Stack_ReleaseMultipleWriteElt(GenericStack_t * pStack, uint16_t nbElementReleased)
{
	uint8_t ReleaseWrite_RetVal_ = 0u;

	pStack->idxWr = (pStack->idxWr + nbElementReleased) % (pStack->sData->szStack);

	pStack->sData->eltSize[pStack->idxSizeWr] = nbElementReleased;
	pStack->idxSizeWr = (pStack->idxSizeWr + 1u) % pStack->sData->szEltSize;

	if(pStack->nbElt < pStack->sData->szStack)
	{
		++pStack->nbElt;
	}

	ReleaseWrite_RetVal_ = 1u;

	return ReleaseWrite_RetVal_;
}

/*******************************************************************************
 * Function Name  : DIA_Init
 * Description    : Initialize DIA module
 * Input          : None.
 * Return         : None.
 *******************************************************************************/
uint16_t Stack_GetNbElements(const GenericStack_t * pStack)
{
	uint16_t retNbElt_ = pStack->nbElt;
	return retNbElt_;
}

/*******************************************************************************
 * Function Name  : Stack_isFull
 * Description    : Check if stack is full
 * Input          : None.
 * Return         : None.
 *******************************************************************************/
uint8_t Stack_isFull(const GenericStack_t * pStack)
{
	return (uint8_t) (pStack->nbElt == pStack->sData->szStack);
}

/*******************************************************************************
 * Function Name  : DIA_Init
 * Description    : Initialize DIA module
 * Input          : None.
 * Return         : None.
 *******************************************************************************/
uint16_t Stack_CheckIntegrity(GenericStack_t * pStack)
{

	pStack->crc = 0u;

#if 0
	return (crc32(0, pStack->sData->stack, pStack->sData->szStack * pStack->sData->szElt) == pStack->crc);
#else
	return 1U;
#endif
}
