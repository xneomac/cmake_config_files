/******************** (C) COPYRIGHT 2015 exoTIC Systems ************************
* File Name          : mem_utils.c
* Description        : TODO
*******************************************************************************/

/* Multi-include protection --------------------------------------------------*/
#ifndef _mem_utils_C_
#define _mem_utils_C_

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "mem_utils.h"

/* Private constants ---------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private types -------------------------------------------------------------*/

/* Private Variables ---------------------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/* Public functions ----------------------------------------------------------*/

void mem_utils_cpy(uint8_t dest[], const uint8_t src[], const uint16_t bytesToCopy)
{
	uint16_t cpy_inc_ = 0u;

	for(cpy_inc_ = 0u; cpy_inc_ < bytesToCopy; cpy_inc_++)
	{
		dest[cpy_inc_] = src[cpy_inc_];
	}
}

uint16_t mem_utils_strlen(const uint8_t str[])
{
	uint16_t str_len_  = 0u;

	while((str_len_ < 0xFFFFu) && (str[str_len_] != 0x00u))
	{
		++str_len_;
	}

	return (str_len_ == 0xFFFFu) ? 0u : str_len_;

}

void mem_utils_set(uint8_t str[], uint8_t defaultValue, const uint16_t bytesToSet)
{
	uint16_t set_inc_ = 0u;

	for(set_inc_ = 0u; set_inc_ < bytesToSet; set_inc_++)
	{
		str[set_inc_] = defaultValue;
	}
}

#ifdef __cplusplus
}
#endif

#endif /* _mem_utils_C_ */
