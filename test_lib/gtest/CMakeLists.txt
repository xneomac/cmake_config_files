#-----------------------------------------------------------------------------------------
# Ext gtest file
#-----------------------------------------------------------------------------------------

set(library_name gtest)
set(url https://googletest.googlecode.com/files/gtest-1.7.0.zip)

#-----------------------------------------------------------------------------------------
# Project configuration
#-----------------------------------------------------------------------------------------

set(external_project_name ${library_name}_builder)

include(ExternalProject)

project(${library_name})

find_package(Threads REQUIRED)

ExternalProject_Add(${external_project_name}
    URL ${url}
    PREFIX ${CMAKE_CURRENT_BINARY_DIR}
    BUILD_COMMAND "make"
    INSTALL_COMMAND ""
)

ExternalProject_Get_Property(${external_project_name} source_dir binary_dir)

add_library(${library_name} IMPORTED STATIC GLOBAL)

set_target_properties(${library_name} PROPERTIES
    "IMPORTED_LOCATION" "${binary_dir}/lib${library_name}.a"
    "IMPORTED_LINK_INTERFACE_LIBRARIES" "${CMAKE_THREAD_LIBS_INIT}"
)

add_custom_target(
	${library_name}_fake_target
	DEPENDS ${external_project_name}
)

set(${library_name}_include_dir "${source_dir}/include" CACHE STRING "${library_name} include directory")
