CMake Config files examples
===========================

This is collection of CMakeLists files to compile a multi executable/library/whatever project

CMake well and enjoy

Compile
-------

```
mkdir build
cd build
cmake ..
make
```

Executable
----------

This is your executable

lib
---

Place into this directory the libraries (no exe) that your project need.

ext
---

Place into this directory the projects that you want to be include automatically into 
the toolchain. The project will be downloaded from the web, compiled and integrated.

Usefull libraries are proposed with the project :
- gtest
- gmock

But off course you can add yours